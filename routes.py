import pathlib
from views import *

PROJECT_ROOT = pathlib.Path(__file__).parent

def setup_routes(app):
    app.router.add_get('/chat', index)
    app.router.add_get('/chat/list', chat_list)
    app.router.add_get('/chat/stats', client_list)
    app.router.add_get('/chat/chat/{chat_id}', get_chat)
    app.router.add_get('/chat/chat/{chat_id}/post', get_form)
    app.router.add_post('/chat/chat/{chat_id}/post', post)
    app.router.add_get('/chat/chat/{chat_id}/stream', chat)
    app.router.add_get('/chat/video', video)
    app.router.add_static('/chat/static',
            path=str(PROJECT_ROOT / 'static'),
            name='static')
